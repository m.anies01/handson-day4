const today = new Date(); // Mengambil tanggal hari ini
let endDate = new Date(today.getFullYear(), today.getMonth() + 1, 0); // Mengset tanggal pada akhir bulan
let loop = new Date(today); // memasukkan tanggal hari ini ke dalam loop 
while (loop <= endDate) { // ketika tanggal pada loop masih bernilai dibawah tanggal akhir, lakukan eksekusi kode dibawah
  console.log(loop); // cetak tanggal pada loop, tanggal hari ini dicetak lebih dahulu,
  let newDate = loop.setDate(loop.getDate() + 1); // membuat hari baru dengan melakukan penambahan
                                                // hari sekarang dengan satu hari
  loop = new Date(newDate); // memasukkan hasil di atas ke dalam loop, kemudian ulangi proses
                            // akan berhenti ketika nilai endDate terpenuhi
}
